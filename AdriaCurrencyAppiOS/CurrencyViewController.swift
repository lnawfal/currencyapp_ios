//
//  CurrencyViewController.swift
//  AdriaCurrencyAppiOS
//
//  Created by DEV-IOS on 8/26/20.
//  Copyright © 2020 DEV-IOS. All rights reserved.
//

import UIKit
import FirebaseAnalytics

struct ExchangeRateAPIConstant {
    
    //General URLs used by the app to load Exchange Rate Data
    static let EXCHANGE_RATE_BASE_URL    = "https://api.exchangeratesapi.io/";
    static let EXCHANGE_RATE_LATEST_URL   =  EXCHANGE_RATE_BASE_URL + "latest?base="  ;
    static let EXCHANGE_RATE_HISTORY_URL  =  EXCHANGE_RATE_BASE_URL + "history?base="  ;

    //The necessary parameters to construct the Exchange Rates History URL
    static let EXCHANGE_RATE_START_DATE_PARAM  =  "&start_at=" ;
    static let EXCHANGE_RATE_END_DATE_PARAM    =  "&end_at=" ;
    static let EXCHANGE_RATE_SYMBOL_PARAM     =  "&symbols=" ;
    
    //The UserDefaults Keys
    static let PREFERENCE_CURRENCY  = "pr_currency";
}

struct Currency {
    
  var abbreviation: String
  var value: Double

init() {
    self.value = 0
    self.abbreviation = "AUD"
    }
    
init(abbr : String, value : Double) {
    self.value = value
    self.abbreviation = abbr
    }
    
}


class CurrencyViewController: UIViewController, NSURLConnectionDataDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var currencyIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var reloadView: UIView!
    @IBOutlet weak var CNoDataLabel: UILabel!
    
    var supportedCurrency: [String] = ["AUD","BGN","BRL","CAD","CHF","CNY","CZK","DKK","EUR","GBP","HKD","HRK","HUF","IDR","ILS","INR","ISK","JPY",
    "KRW","MXN","MYR","NOK","NZD","PLN","RON","RUB","SEK","SGD","THB","TRY","USD","ZAR"]
    
    var model = [Currency]()
    var selectedItem = Currency()
    var base : String = "EUR"
    
    var barAccessory : UIToolbar!
    var historyViewController : HistoryViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        if(UserDefaults.standard.string(forKey: ExchangeRateAPIConstant.PREFERENCE_CURRENCY) != nil){
            base = UserDefaults.standard.string(forKey: ExchangeRateAPIConstant.PREFERENCE_CURRENCY) ?? "EUR"
        }else{
            UserDefaults.standard.set("EUR", forKey: ExchangeRateAPIConstant.PREFERENCE_CURRENCY)
        }
        
        self.navigationItem.title = "Currency App"
        currencyLabel.text = base
        
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        
        // Toolbar
        let btnDone = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(Done))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Annuler", style: .plain, target: self, action: #selector(Cancel))
        barAccessory = UIToolbar(frame: CGRect(x: 0, y: 900, width: self.view.frame.width, height: 44))
        barAccessory.barStyle = .default
        barAccessory.isTranslucent = false
        barAccessory.items = [cancelButton, spaceButton, btnDone]
        barAccessory.isUserInteractionEnabled = true
        barAccessory.sizeToFit()
        self.view.addSubview(barAccessory)
    
        self.getExchangeRateJSON(base)
        
        sendScreenView("CurrencyListView")
    }
    
    
    @objc func pushViewController(){
           sendScreenView("HistoryExchangeRate")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           historyViewController = storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as? HistoryViewController
           historyViewController.symbol = selectedItem.abbreviation
           self.navigationController?.pushViewController(historyViewController, animated: true)
       }
    
    @IBAction func relodExchangeData(_ sender: AnyObject) {
        self.getExchangeRateJSON(base)
    }
    
    @IBAction func optionsButtonTapped(_ sender: Any) {
     UIView.animate(withDuration: 0.5, animations: {
        self.barAccessory.frame.origin = CGPoint(x: 0, y: self.view.frame.height - self.picker.frame.height - self.barAccessory.frame.height)
        self.picker.frame.origin = CGPoint(x: 0, y: self.view.frame.height - self.picker.frame.height)
        }, completion: nil)
    }
    
    @objc func Done(sender:UIBarButtonItem){
         UserDefaults.standard.set(self.base, forKey: ExchangeRateAPIConstant.PREFERENCE_CURRENCY)
         self.currencyLabel.text = self.base
         self.getExchangeRateJSON(self.base)
        UIView.animate(withDuration: 0.5, animations: {
             self.barAccessory.frame.origin = CGPoint(x: 0, y: 900)
             self.picker.frame.origin = CGPoint(x: 0, y: 1000)
         }, completion: nil)
    }
    
    @objc func Cancel(sender:UIBarButtonItem){
        UIView.animate(withDuration: 0.5, animations: {
            self.barAccessory.frame.origin = CGPoint(x: 0, y: 900)
            self.picker.frame.origin = CGPoint(x: 0, y: 1000)
        }, completion: nil)
    }
       
    
    // MARK: - Table View
    
    @objc(numberOfSectionsInTableView:) func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    
    @objc(tableView:didSelectRowAtIndexPath:) func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: true)
           selectedItem = model[indexPath.row]
           self.pushViewController()
       }
       
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell") as? CurrencyCell else { return UITableViewCell() }
        
        cell.titleLabel.text = model[indexPath.row].abbreviation
        cell.valueLabel.text = String(model[indexPath.row].value)
        
        return cell
    }
    
    // MARK: - Picker View
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return supportedCurrency.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return supportedCurrency[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        base = supportedCurrency[row]
    }
    
    // MARK: - Loding ExchangeRate Data
    
    fileprivate func getExchangeRateJSON(_ base: String) {

       self.currencyIndicator.startAnimating()
       self.currencyIndicator.isHidden = false
       self.tableView.isHidden = true
       self.reloadView.isHidden = true
        
       self.model.removeAll()
        
       let urlPath: String = ExchangeRateAPIConstant.EXCHANGE_RATE_LATEST_URL + base
       print(urlPath)
       guard let url: URL = URL(string: urlPath) else { return }
        
       let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
       guard let dataResponse = data, error == nil else {
                     print(error?.localizedDescription ?? "Response Error")
                     DispatchQueue.main.async {
                        self.currencyIndicator.stopAnimating()
                        self.currencyIndicator.isHidden = true
                        self.reloadView.isHidden = false 
                    }
                        return
                }
            
             do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:dataResponse, options: [])
                print(jsonResponse)
                if let ExchangeRateObject = jsonResponse as? [String: Any] {
                let ratesObject =  ExchangeRateObject["rates"] as? [String:Any]
                    for i in (0..<self.supportedCurrency.count){
                        if let value = ratesObject?[self.supportedCurrency[i]] as? Double {
                            if(value != 1){
                                self.model.append(Currency(abbr:self.supportedCurrency[i], value: value))
                            }
                        }
                    }
                
                DispatchQueue.main.async {
                self.currencyIndicator.stopAnimating()
                self.currencyIndicator.isHidden = true
                self.tableView.reloadData()
                    
                    if(self.model.count == 0){
                        self.reloadView.isHidden = false
                    }else{
                        self.reloadView.isHidden = true
                        self.tableView.isHidden = false
                    }
                    }
                }
             } catch let parsingError {
                print("Error", parsingError)
           }
        }
        task.resume()
    }
    
    func sendScreenView(_ screenName:String){
           Analytics.setAnalyticsCollectionEnabled(true)
           Analytics.logEvent(AnalyticsEventSelectContent, parameters: [AnalyticsParameterItemID : screenName])
       }
}
