//
//  HistoryViewController.swift
//  AdriaCurrencyAppiOS
//
//  Created by DEV-IOS on 8/27/20.
//  Copyright © 2020 DEV-IOS. All rights reserved.
//

import UIKit
import Charts

class HistoryViewController: UIViewController, NSURLConnectionDataDelegate, ChartViewDelegate {
    
    @IBOutlet weak var historyTitleLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var historyIndicator: UIActivityIndicatorView!
    @IBOutlet var chartView: LineChartView!
    @IBOutlet weak var HreloadView: UIView!
    @IBOutlet weak var HNoDataLabel: UILabel!
    
    var symbol : String = "AUD";
    var base : String = "EUR"
    
    var currencyValues = [Double]()
    var dates = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        chartView.delegate = self
        
        if(UserDefaults.standard.string(forKey: ExchangeRateAPIConstant.PREFERENCE_CURRENCY) != nil){
            base = UserDefaults.standard.string(forKey: ExchangeRateAPIConstant.PREFERENCE_CURRENCY) ?? "EUR"
        }
        
        currencyLabel.text = String(format:"1 %@    >>    %@", base, symbol)
        self.navigationItem.title = "Currency App"
        let button: UIButton = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named:"back_button.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(dismissView), for: UIControl.Event.touchUpInside)
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
        
        self.createLineChartView()
        self.getExchangeHistoryJSON()
    }

     @objc func dismissView(sender:UIBarButtonItem) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func relodHistoryData(_ sender: AnyObject) {
        self.getExchangeHistoryJSON()
    }
    
    fileprivate func getExchangeHistoryJSON() {
        
         self.historyIndicator.startAnimating()
         self.historyIndicator.isHidden = false
         self.chartView.isHidden = true
         self.HreloadView.isHidden = true
          
         let urlPath: String = ConstructExchangeHistoryUrl(symbol)
         print(urlPath)
         guard let url: URL = URL(string: urlPath) else { return }
          
         let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
         guard let dataResponse = data, error == nil else {
                       print(error?.localizedDescription ?? "Response Error")
                       DispatchQueue.main.async {
                          self.historyIndicator.stopAnimating()
                          self.historyIndicator.isHidden = true
                          self.HreloadView.isHidden = false
                      }
                          return
                  }
              
               do{
                  //here dataResponse received from a network request
                  let jsonResponse = try JSONSerialization.jsonObject(with:dataResponse, options: [])
                  print(jsonResponse)
                  if let ExchangeRateObject = jsonResponse as? [String: Any] {
                  let ratesObject =  ExchangeRateObject["rates"] as? [String:Any]
                      
                    let datesList: [String] = self.getDatesList()
                
                      for i in (0..<datesList.count){
                          if let currencyObject = ratesObject?[datesList[i]] as? [String:Any] {
                           // if(i%4 == 0){
                               self.dates.append(datesList[i])
                               self.currencyValues.append((currencyObject[self.symbol] as? Double)!)
                           // }
                          }
                      }
                  
                   DispatchQueue.main.async {
                      self.historyIndicator.stopAnimating()
                      self.historyIndicator.isHidden = true
                      if(self.currencyValues.count == 0){
                          self.HreloadView.isHidden = false
                      }else{
                          self.HreloadView.isHidden = true
                          self.chartView.isHidden = false
                          self.setDataCount(self.dates, values: self.currencyValues)
                      }
                    }
                  }
                  
               } catch let parsingError {
                  print("Error", parsingError)
             }
          }
          task.resume()
    }
    
     func createLineChartView(){
    
        chartView.chartDescription?.enabled = false
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = true
        
        // x-axis limit line
        let llXAxis = ChartLimitLine(limit: 10, label: "Index 10")
        llXAxis.lineWidth = 4
        llXAxis.lineDashLengths = [10, 10, 0]
        llXAxis.labelPosition = .bottomRight
        llXAxis.valueFont = .systemFont(ofSize: 10)
        
        chartView.xAxis.gridLineDashLengths = [10, 10]
        chartView.xAxis.gridLineDashPhase = 0
        
        
        let leftAxis = chartView.leftAxis
        leftAxis.removeAllLimitLines()
        leftAxis.gridLineDashLengths = [5, 5]
        leftAxis.drawLimitLinesBehindDataEnabled = true
        leftAxis.labelTextColor = .white
        
        let l = chartView.legend
        l.form = .line
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.textColor = .white
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        
        chartView.rightAxis.enabled = false

        let marker = BalloonMarker(color: UIColor(red: 103/255, green: 110/255, blue: 129/255, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView.marker = marker
        
        chartView.legend.form = .line
        
        chartView.animate(xAxisDuration: 2.5)
    }
    
    func setDataCount(_ dates: [String], values: [Double]) {
        let values = (0..<dates.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: values[i])
        }
        
        var half_diff : Double = 0
        half_diff = ( self.currencyValues.max()! - self.currencyValues.min()!)/2
        
        let xAxis = chartView.xAxis
        xAxis.labelRotationAngle = 270
        xAxis.labelPosition = .bottom
        xAxis.valueFormatter = self
        xAxis.labelTextColor = .white
        
        let leftAxis = chartView.leftAxis
        
        if(half_diff > 0){
          leftAxis.axisMaximum = self.currencyValues.max()! + half_diff
          leftAxis.axisMinimum = self.currencyValues.min()! - half_diff
        }else{
            leftAxis.axisMaximum = self.currencyValues.max()! + 1
            leftAxis.axisMinimum = self.currencyValues.min()! - 1
        }
        

        
        let set1 = LineChartDataSet(entries: values, label: "Dates")
        set1.drawIconsEnabled = false
        
       // set1.lineDashLengths = [5, 2.5]
       // set1.highlightLineDashLengths = [5, 2.5]
        set1.setColor(.white)
        set1.setCircleColor(.white)
        set1.lineWidth = 1
        set1.circleRadius = 3
        set1.drawCircleHoleEnabled = true
        set1.valueFont = .systemFont(ofSize: 9)
        set1.formLineDashLengths = [5, 2.5]
        set1.formLineWidth = 1
        set1.formSize = 15
       // set1.setDrawHighlightIndicators(false)
       // set1.drawHorizontalHighlightIndicatorEnabled = false
        
        set1.setColor(UIColor(red: 121/255, green: 162/255, blue: 175/255, alpha: 1))
        set1.fillColor = UIColor(red: 121/255, green: 162/255, blue: 175/255, alpha: 1)
        
        set1.drawFilledEnabled = true
        
        let data = LineChartData(dataSet: set1)
        data.setValueTextColor(.white)
        data.setDrawValues(false)
        
        chartView.data = data
    }
    
    fileprivate func ConstructExchangeHistoryUrl(_ symbol: String)  -> String {
     
        return String(format:"%@%@%@%@%@%@%@%@",ExchangeRateAPIConstant.EXCHANGE_RATE_HISTORY_URL, base ,
        ExchangeRateAPIConstant.EXCHANGE_RATE_START_DATE_PARAM,  getStartDate(),
        ExchangeRateAPIConstant.EXCHANGE_RATE_END_DATE_PARAM,  self.getEndDate(),
        ExchangeRateAPIConstant.EXCHANGE_RATE_SYMBOL_PARAM, symbol)
    }
    
    // MARK : GET TODAY YESTERDAY and TOMORROW DATE FORMAT
    
    func getEndDate() -> String{
        let todaysDate:Date = Date()
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newDate = todaysDate.addingTimeInterval(TimeInterval(60 * 60 * 24 * 0))
        let todayString:String = dateFormatter.string(from: newDate)
        return todayString;
    }
    
    func getStartDate() -> String{
        let todaysDate:Date = Date()
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fromDate = Calendar.current.date(byAdding: .month, value: -2, to: todaysDate)
        let todayString:String = dateFormatter.string(from: fromDate ?? Date())
        return todayString;
    }
    
    func getDatesList() -> [String] {

        var dates = [String]()
        let endDate:Date  = Date()
        let startDate = Calendar.current.date(byAdding: .month, value: -2, to: endDate)!
        
        let components = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        let numberOfDays = components.day ?? 0
        
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for i in 0...numberOfDays {
            let nextDate = Calendar.current.date(byAdding: .day, value: i, to: startDate)
            dates.append(dateFormatter.string(from: nextDate ?? Date()))
        }
        
        return dates
    }
}

extension HistoryViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return dates[Int(value) % dates.count]
    }
}

