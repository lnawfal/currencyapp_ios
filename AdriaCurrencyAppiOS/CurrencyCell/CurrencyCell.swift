//
//  CurrencyCell.swift
//  AdriaCurrencyAppiOS
//
//  Created by DEV-IOS on 8/26/20.
//  Copyright © 2020 DEV-IOS. All rights reserved.
//

import UIKit

class CurrencyCell : UITableViewCell {
    
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var containerView: UIView!
    
  @IBInspectable var cornerRadius: CGFloat = 10
  @IBInspectable var shadowOffsetWidth: Int = 0
  @IBInspectable var shadowOffsetHeight: Int = 3
  @IBInspectable var shadowColor: UIColor? = UIColor.white
  @IBInspectable var shadowOpacity: Float = 0.5
    
  override func awakeFromNib() {
        super.awakeFromNib()
    }
       
  override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
  override func layoutSubviews() {

    
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.masksToBounds = false
     // containerView.layer.shadowColor = shadowColor?.cgColor
        containerView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        containerView.layer.shadowOpacity = shadowOpacity
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:1.0).cgColor

    }
}
